<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Sju_Hhtoday
 * @subpackage Sju_Hhtoday/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Sju_Hhtoday
 * @subpackage Sju_Hhtoday/includes
 * @author     Your Name <email@example.com>
 */
class Sju_Hhtoday_Activator {

	/**
	 * Add ACF Fields.
	 *
	 * This function adds the necessary ACF fields.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {



	}

}