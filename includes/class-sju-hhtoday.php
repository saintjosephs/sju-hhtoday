<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Sju_Hhtoday
 * @subpackage Sju_Hhtoday/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Sju_Hhtoday
 * @subpackage Sju_Hhtoday/includes
 * @author     Your Name <email@example.com>
 */
class Sju_Hhtoday {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Sju_Hhtoday_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $sju_hhtoday    The string used to uniquely identify this plugin.
	 */
	protected $sju_hhtoday;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->sju_hhtoday = 'sju-hhtoday';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		
		
		$this->sju_register_field_groups();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Sju_Hhtoday_Loader. Orchestrates the hooks of the plugin.
	 * - Sju_Hhtoday_i18n. Defines internationalization functionality.
	 * - Sju_Hhtoday_Admin. Defines all hooks for the admin area.
	 * - Sju_Hhtoday_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sju-hhtoday-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sju-hhtoday-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-sju-hhtoday-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-sju-hhtoday-public.php';

		$this->loader = new Sju_Hhtoday_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Sju_Hhtoday_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Sju_Hhtoday_i18n();
		$plugin_i18n->set_domain( $this->sju_hhtoday );

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Sju_Hhtoday_Admin( $this->sju_hhtoday, $this->version );

		$this->loader->add_action( 'init', $plugin_admin, 'sju_hhtoday_cpts' );
		$this->loader->add_action( 'init', $plugin_admin, 'sju_hhtoday_custom_post_status' );
    $this->loader->add_action( 'admin_footer-post.php', $plugin_admin, 'sju_hhtoday_append_post_status_list');
    $this->loader->add_action( 'display_post_states', $plugin_admin, 'sju_hhtoday_display_archive_state' );    

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Sju_Hhtoday_Public( $this->sju_hhtoday, $this->version );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		

    
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}


	public function sju_register_field_groups() {
  	if(function_exists("register_field_group")) {
    register_field_group(array (
  		'id' => 'acf_hh-today-fields',
  		'title' => 'HH Today Fields',
  		'fields' => array (
  			array (
  				'key' => 'field_5655c55a1436a',
  				'label' => 'Website',
  				'name' => 'website',
  				'type' => 'text',
  				'default_value' => '',
  				'placeholder' => '',
  				'prepend' => '',
  				'append' => '',
  				'formatting' => 'html',
  				'maxlength' => '',
  			),
  			array (
  				'key' => 'field_5655c5701436b',
  				'label' => 'Image',
  				'name' => 'image',
  				'type' => 'image',
  				'save_format' => 'id',
  				'preview_size' => 'thumbnail',
  				'library' => 'uploadedTo',
  			),
  			array (
  				'key' => 'field_5655c5a41436c',
  				'label' => 'File Upload',
  				'name' => 'file',
  				'type' => 'file',
  				'save_format' => 'id',
  				'library' => 'all',
  			),
  			array (
  				'key' => 'field_565da79b8a8e3',
  				'label' => 'Classifieds ID',
  				'name' => 'classifieds_id',
  				'type' => 'number',
  				'instructions' => 'This field should be blank on the ADVERT content type, and on the HHTODAY content type with the post id of the advert from which the hhtoday post was generated.	This is used for pulling the full content (rather than the excerpt) into the HHToday template, since we are only using a trimmed version of the full content in the HHToday email.',
  				'default_value' => '',
  				'placeholder' => '',
  				'prepend' => '',
  				'append' => '',
  				'min' => '',
  				'max' => '',
  				'step' => '',
  			),
  		),
  		'location' => array (
  			array (
  				array (
  					'param' => 'post_type',
  					'operator' => '==',
  					'value' => 'advert',
  					'order_no' => 0,
  					'group_no' => 0,
  				),
  			),
  			array (
  				array (
  					'param' => 'post_type',
  					'operator' => '==',
  					'value' => 'hhtoday',
  					'order_no' => 0,
  					'group_no' => 1,
  				),
  			),
  		),
  		'options' => array (
  			'position' => 'normal',
  			'layout' => 'default',
  			'hide_on_screen' => array (
  				0 => 'categories',
  			),
  		),
  		'menu_order' => 0,
  	));
  	register_field_group(array (
  		'id' => 'acf_hhtoday-submission-related-fields',
  		'title' => 'HHToday Submission Related Fields',
  		'fields' => array (
  			array (
  				'key' => 'field_5655c10918b92',
  				'label' => 'Contact Name',
  				'name' => 'contact_name',
  				'type' => 'text',
  				'default_value' => '',
  				'placeholder' => '',
  				'prepend' => '',
  				'append' => '',
  				'formatting' => 'html',
  				'maxlength' => '',
  			),
  			array (
  				'key' => 'field_5655c12c18b93',
  				'label' => 'Contact Email',
  				'name' => 'contact_email',
  				'type' => 'text',
  				'default_value' => '',
  				'placeholder' => '',
  				'prepend' => '',
  				'append' => '',
  				'formatting' => 'html',
  				'maxlength' => '',
  			),
  			array (
  				'key' => 'field_5655c6cbbd545',
  				'label' => 'Start Date',
  				'name' => 'start_date',
  				'type' => 'text',
  				'default_value' => '',
  				'placeholder' => '',
  				'prepend' => '',
  				'append' => '',
  				'formatting' => 'html',
  				'maxlength' => '',
  			),
  			array (
  				'key' => 'field_5655c6d8bd546',
  				'label' => 'End Date',
  				'name' => 'end_date',
  				'type' => 'text',
  				'default_value' => '',
  				'placeholder' => '',
  				'prepend' => '',
  				'append' => '',
  				'formatting' => 'html',
  				'maxlength' => '',
  			),
  		),
  		'location' => array (
  			array (
  				array (
  					'param' => 'post_type',
  					'operator' => '==',
  					'value' => 'hhtoday',
  					'order_no' => 0,
  					'group_no' => 0,
  				),
  			),
  			array (
  				array (
  					'param' => 'post_type',
  					'operator' => '==',
  					'value' => 'advert',
  					'order_no' => 0,
  					'group_no' => 1,
  				),
  			),
  		),
  		'options' => array (
  			'position' => 'normal',
  			'layout' => 'default',
  			'hide_on_screen' => array (
  			),
  		),
  		'menu_order' => 0,
  	));
}
  }
	

}