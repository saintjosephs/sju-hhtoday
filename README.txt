=== SJU Hawk Hill Today Plugin ===
Contributors: kellygrape
Tags: custom, university, newsletter
Requires at least: 4.3.1
Tested up to: 4.3.1
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Here is a short description of the plugin.  This should be no more than 150 characters.  No markup here.