<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.sju.edu
 * @since      1.0.0
 *
 * @package    Sju_Hhtoday
 * @subpackage Sju_Hhtoday/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
