<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.sju.edu
 * @since      1.0.0
 *
 * @package    Sju_Hhtoday
 * @subpackage Sju_Hhtoday/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Sju_Hhtoday
 * @subpackage Sju_Hhtoday/admin
 * @author     Kelly Anne Pipe <kpipe@sju.edu>
 */
class Sju_Hhtoday_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}


  /**
	 * Creates a new custom post type HHToday
	 *
	 * @since 	1.0.0
	 * @access 	public
	 * @uses 	register_post_type()
	 */
	public function sju_hhtoday_cpts() {
		

  	$hhtodaylabels = array(
  		'name'                  => _x( 'HH Today Posts', 'Post Type General Name', 'text_domain' ),
  		'singular_name'         => _x( 'HH Today Post', 'Post Type Singular Name', 'text_domain' ),
  		'menu_name'             => __( 'HH Today Post', 'text_domain' ),
  		'name_admin_bar'        => __( 'HH Today Post', 'text_domain' ),
  		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
  		'all_items'             => __( 'All Items', 'text_domain' ),
  		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
  		'add_new'               => __( 'Add New', 'text_domain' ),
  		'new_item'              => __( 'New Item', 'text_domain' ),
  		'edit_item'             => __( 'Edit Item', 'text_domain' ),
  		'update_item'           => __( 'Update Item', 'text_domain' ),
  		'view_item'             => __( 'View Item', 'text_domain' ),
  		'search_items'          => __( 'Search Item', 'text_domain' ),
  		'not_found'             => __( 'Not found', 'text_domain' ),
  		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
  		'items_list'            => __( 'Items list', 'text_domain' ),
  		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
  		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
  	);
  	$hhtodayargs = array(
  		'label'                 => __( 'HH Today Post', 'text_domain' ),
  		'description'           => __( 'Post of the Hawk Hill Today Type.  Used to feed the MailPoet Newsletter.  Posts of this type are generated based on the Classifieds fields.', 'text_domain' ),
  		'labels'                => $hhtodaylabels,
  		'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', ),
  		'taxonomies'            => array( 'category', 'hhtoday_categories' ),
  		'hierarchical'          => false,
  		'public'                => true,
  		'show_ui'               => true,
  		'show_in_menu'          => true,
  		'menu_position'         => 5,
  		'show_in_admin_bar'     => true,
  		'show_in_nav_menus'     => true,
  		'can_export'            => true,
  		'has_archive'           => true,		
  		'exclude_from_search'   => false,
  		'publicly_queryable'    => true,
  		'capability_type'       => 'page',
  	);
  	register_post_type( 'hhtoday', $hhtodayargs );

    $hheventlabels = array(
  		'name'                  => _x( 'HH Today Events', 'Post Type General Name', 'text_domain' ),
  		'singular_name'         => _x( 'HH Today Event', 'Post Type Singular Name', 'text_domain' ),
  		'menu_name'             => __( 'HH Today Event', 'text_domain' ),
  		'name_admin_bar'        => __( 'HH Today Event', 'text_domain' ),
  		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
  		'all_items'             => __( 'All Items', 'text_domain' ),
  		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
  		'add_new'               => __( 'Add New', 'text_domain' ),
  		'new_item'              => __( 'New Item', 'text_domain' ),
  		'edit_item'             => __( 'Edit Item', 'text_domain' ),
  		'update_item'           => __( 'Update Item', 'text_domain' ),
  		'view_item'             => __( 'View Item', 'text_domain' ),
  		'search_items'          => __( 'Search Item', 'text_domain' ),
  		'not_found'             => __( 'Not found', 'text_domain' ),
  		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
  		'items_list'            => __( 'Items list', 'text_domain' ),
  		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
  		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
  	);
  	$hheventargs = array(
  		'label'                 => __( 'HH Today Event', 'text_domain' ),
  		'labels'                => $hheventlabels,
  		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields', ),
  		'taxonomies'            => array( 'category' ),
  		'hierarchical'          => false,
  		'public'                => true,
  		'show_ui'               => true,
  		'show_in_menu'          => true,
  		'menu_position'         => 5,
  		'show_in_admin_bar'     => true,
  		'show_in_nav_menus'     => true,
  		'can_export'            => true,
  		'has_archive'           => true,		
  		'exclude_from_search'   => false,
  		'publicly_queryable'    => true,
  		'capability_type'       => 'page',
  	);
  	register_post_type( 'hhtoday_event', $hheventargs );
  	
  	$hhtaxlabels = array(
  		'name'                       => _x( 'HH Today Category', 'Taxonomy General Name', 'text_domain' ),
  		'singular_name'              => _x( 'HH Today Categories', 'Taxonomy Singular Name', 'text_domain' ),
  		'menu_name'                  => __( 'HH Today Category', 'text_domain' ),
  		'all_items'                  => __( 'All Items', 'text_domain' ),
  		'parent_item'                => __( 'Parent Item', 'text_domain' ),
  		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
  		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
  		'add_new_item'               => __( 'Add New Item', 'text_domain' ),
  		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
  		'update_item'                => __( 'Update Item', 'text_domain' ),
  		'view_item'                  => __( 'View Item', 'text_domain' ),
  		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
  		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
  		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
  		'popular_items'              => __( 'Popular Items', 'text_domain' ),
  		'search_items'               => __( 'Search Items', 'text_domain' ),
  		'not_found'                  => __( 'Not Found', 'text_domain' ),
  		'items_list'                 => __( 'Items list', 'text_domain' ),
  		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
  	);
  	$hhtaxargs = array(
  		'labels'                     => $hhtaxlabels,
  		'hierarchical'               => true,
  		'public'                     => true,
  		'show_ui'                    => true,
  		'show_admin_column'          => true,
  		'show_in_nav_menus'          => true,
  		'show_tagcloud'              => true,
  	);
  	register_taxonomy( 'hhtoday_categories', array( 'hhtoday','advert' ), $hhtaxargs );


	} // new_cpt_jobs()
	
	
	/**
	 * Creates a new custom post statuses
	 *
	 * @since 	1.0.0
	 * @access 	public
	 * @uses 	register_post_type()
	 */
	public function sju_hhtoday_custom_post_status(){
    register_post_status( 'archive', array(
      'label'                     => _x( 'Archive', 'advert' ),
      'public'                    => false,
  		'exclude_from_search'       => true,
  		'show_in_admin_all_list'    => true,
  		'show_in_admin_status_list' => true,
      'label_count'               => _n_noop( 'Archive <span class="count">(%s)</span>', 'Archive <span class="count">(%s)</span>' )
    ));
    register_post_status( 'rejected', array(
      'label'                     => _x( 'Rejected', 'advert' ),
      'public'                    => false,
  		'exclude_from_search'       => true,
  		'show_in_admin_all_list'    => true,
  		'show_in_admin_status_list' => true,
      'label_count'               => _n_noop( 'Rejected <span class="count">(%s)</span>', 'Rejected <span class="count">(%s)</span>' )
    ));
  }
  
  public function sju_hhtoday_append_post_status_list(){
    global $post;
    $complete = '';
    $label = '';
    if($post->post_type == 'advert'){
      if($post->post_status == 'archive'){
       $complete = ' selected=\"selected\"';
       $label = '<span id=\"post-status-display\"> Archived</span>';
      }
      if($post->post_status == 'rejected'){
       $rej_complete = ' selected=\"selected\"';
       $rej_label = '<span id=\"post-status-display\"> Rejected</span>';
      }
      echo '
      <script>
      jQuery(document).ready(function($){
       $("select#post_status").append("<option value=\"archive\" '.$complete.'>Archive</option>");
       $(".misc-pub-section label").append("'.$label.'");
       $("select#post_status").append("<option value=\"rejected\" '.$rej_complete.'>Rejected</option>");
       $(".misc-pub-section label").append("'.$rej_label.'");
      });
      </script>
      ';
    }
  }

  public function sju_hhtoday_display_archive_state( $states ) {
    global $post;
    $arg = get_query_var( 'post_status' );
    if($arg != 'archive'){
      if($post->post_status == 'archive'){
        return array('Archive');
      }
    }
    if($arg != 'rejected'){
      if($post->post_status == 'rejected'){
        return array('Rejected');
      }
    }
    return $states;
  }



}