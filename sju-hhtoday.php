<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.sju.edu
 * @since             1.0.0
 * @package           Sju_Hhtoday
 *
 * @wordpress-plugin
 * Plugin Name:       SJU Hawk Hill Today Plugin
 * Plugin URI:        http://nestlist.sju.edu/hhtoday
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Kelly Anne Pipe
 * Author URI:        http://www.sju.edu
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       sju-hhtoday
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-sju-hhtoday-activator.php
 */
function activate_sju_hhtoday() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sju-hhtoday-activator.php';
	Sju_Hhtoday_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-sju-hhtoday-deactivator.php
 */
function deactivate_sju_hhtoday() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sju-hhtoday-deactivator.php';
	Sju_Hhtoday_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_sju_hhtoday' );
register_deactivation_hook( __FILE__, 'deactivate_sju_hhtoday' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-sju-hhtoday.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_sju_hhtoday() {

	$plugin = new Sju_Hhtoday();
	$plugin->run();

}
run_sju_hhtoday();
